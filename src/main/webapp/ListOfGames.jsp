<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <title>List of Games</title>
</head>
<body>

<h1 style="text-align:center"> Game List </h1>

<h2 style="text-align:center"> <%=request.getParameter("name") %> can  either create the game, or join existing one. </h2>

<div style="margin:auto;width:800px" >
    <div style="float: left" class="w3-container">
        <h2>Waiting games</h2>
        <ul>
            <c:forEach items="${listOfWaitingGames}" var="Id">
                <li><a href="/rock-paper-scissors/Waiting?id=${Id}&name=${currentPlayerName}">
                    <c:out value="Game number ${Id}"/>
                </a></li>
            </c:forEach>
        </ul>
    </div>


    <div style="float: left" class="w3-container">
        <h2>Playing games</h2>
        <ul>
            <c:forEach items="${listOfPlayingGames}" var="Id">
                <li><a href="/rock-paper-scissors/Play?id=${Id}&name=${currentPlayerName}">
                    <c:out value="Game number ${Id}"/>
                </a></li>
            </c:forEach>
        </ul>
    </div>


    <div class="w3-container" style="float: left" >
        <h2>Created games</h2>
        <ul>
            <c:forEach items="${listOfCreatedGames}" var="Id">
                <li><a href="/rock-paper-scissors/Created?id=${Id}&name=${currentPlayerName}">
                    <c:out value="Game number ${Id}"/>
                </a></li>
            </c:forEach>
        </ul>

        <div class="w3-container">
            <a href="/rock-paper-scissors/GameCreation?name=${currentPlayerName}" ><button class="w3-btn">
                Create new game</button></a>
        </div>

    </div>

</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div class="w3-container" style="float:clear">
    <a href="/rock-paper-scissors/List?name=${currentPlayerName}"><button class="w3-btn" >Refresh</button></a>
</div>
</body>
</html>