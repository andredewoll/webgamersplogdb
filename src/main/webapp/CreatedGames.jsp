<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
  <title>Game has been created</title>
</head>
<body>


<br/>
<h1 style="text-align:center">Nobody wants to play with you yet, go to the list of games and try to refresh</h1>
<br>
<h2 style="text-align:center">Meanwhile, you can see our rules again </h2>
<div style="text-align:center">
  <img alt="sps.jpg" src="img/sps.jpg">
</div>
<br>

<div>
  <a href="/rock-paper-scissors/List?name=${name}">
    <button class="w3-btn">Return to games list</button>
  </a>
</div>

</body>
</html>