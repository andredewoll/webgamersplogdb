<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <title>Playing Game</title>

</head>
<body>

<h1 style="text-align:center">RockScissorsPaper game number ${id}</h1>
<h2 style="text-align:center">${currentPlayerName} against ${enemy}</h2>
<h2 style="text-align:center">The game is started? -  ${isStarted}</h2>
<h2 style="text-align:center">Game info -  ${gameState}</h2>

<div style="text-align:center">
    <form action="MakeTurn" method="post"  class="w3-container w3-card-4">
        <h3>Choose Your turn, Sir!</h3>
        <input type="radio" name="turn" value="ROCK" class="w3-radio">ROCK&nbsp;&nbsp;
        <input type="radio" name="turn" value="SCISSORS" class="w3-radio">SCISSORS&nbsp;&nbsp;
        <input type="radio" name="turn" value="PAPER" class="w3-radio">PAPER<br>
        <input type="hidden" name="id" value="${id}"/>
        <input type="hidden" name = "currentPlayerName" value="${currentPlayerName}">
        <input type="hidden" name = "enemy" value="${enemy}">
        <input type="hidden" name = "winner" value="${winner}">
        <input type="hidden" name = "resultCode" value="${resultCode}">
        <input type="hidden" name = "isStarted" value="${isStarted}"> <br>
        <input type="submit" value="Make turn" class="w3-btn">
    </form>
</div>


<h1 style="text-align:center;color:red" >The Winner is  ${winner}</h1>
<br>


<div class="w3-container w3-center">
    <a href="/rock-paper-scissors/Play?id=${id}&name=${currentPlayerName}&enemy=${enemy}">
        <button class="w3-btn">Refresh</button>
    </a>
</div>
<br>
<div class="w3-container w3-center">
    <a href="/rock-paper-scissors/List?id=${id}&name=${currentPlayerName}">
        <button class="w3-btn">Back to games</button>
    </a>
</div>

</body>
</html>