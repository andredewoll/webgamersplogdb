<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
  <title>Do you want to play?</title>
</head>
<body>

<h1 style="text-align:center">Game number ${id}</h1>

<h2 style="text-align:center">${name}, do you want to play with ${enemy}?</h2>
<br>

<div class="w3-container w3-center" style="width:300px; margin:auto" >

  <a href="/rock-paper-scissors/List?name=${name}">
    <button class="w3-btn" style="float:right">No</button>
  </a>

  <a href="/rock-paper-scissors/Play?id=${id}&name=${name}&enemy=${enemy}">
    <button class="w3-btn" style="float:left">Yes</button>
  </a>

</div>
</body>
</html>