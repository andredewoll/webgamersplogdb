DROP TABLE IF EXISTS players;
DROP SEQUENCE IF EXISTS global_seq;
DROP TYPE IF EXISTS player_gender;

CREATE SEQUENCE global_seq START 101;
CREATE TYPE player_gender AS ENUM ('MALE', 'FEMALE');

CREATE TABLE players
(
  id                BIGINT PRIMARY KEY DEFAULT nextval('global_seq'),
  login             VARCHAR UNIQUE NOT NULL,
  password          VARCHAR NOT NULL,
  email             VARCHAR UNIQUE NOT NULL,
  fullname          VARCHAR,
  gender            player_gender,
  birthday_date     DATE,
  registration_time TIMESTAMP,
  about             VARCHAR,
  avatar            VARCHAR
);