package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.model.Gender;
import com.softserveinc.ita.multigame.model.Player;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PlayerDao {

    //TODO cut this, make tests
    public static void main(String[] args) {
        Player newPlayer = new Player("1112", "1234", "email2");
        Player vasya = new Player("Vasya111", "123456", "Vaaaaaasya@mail.com");
        vasya.setId(101L);
//        System.out.println(new PlayerDao().create(newPlayer));
        System.out.println(new PlayerDao().update(vasya));
        System.out.println(new PlayerDao().delete(102L));
        System.out.println(new PlayerDao().get(101L));
        System.out.println(new PlayerDao().getAll());
    }

    public int create(Player player){
        try(Connection con = getConnection();
            PreparedStatement pstmt = con.prepareStatement("INSERT INTO players " +
                    "(login, password, email, fullname, gender, birthday_date, registration_time, about, avatar)" +
                    "VALUES (? , ? , ?, ?, CAST(? AS player_gender), ?, ?, ? ,?)")
        ){
            pstmt.setString(1, player.getLogin());
            pstmt.setString(2, player.getPassword());
            pstmt.setString(3, player.getEmail());
            pstmt.setString(4, player.getFullName());
            pstmt.setString(5, player.getGender()!=null ? player.getGender().toString() : null);
            pstmt.setDate(6, player.getBirthdayDate()!=null ?  Date.valueOf(player.getBirthdayDate()) : null);
            pstmt.setTimestamp(7, player.getRegistrationTime()!=null ? Timestamp.valueOf(player.getRegistrationTime()) : null);
            pstmt.setString(8, player.getAbout());
            pstmt.setString(9, player.getAvatar());
            return pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int update(Player player){
        try(Connection con = getConnection();
            PreparedStatement pstmt = con.prepareStatement("UPDATE players SET login=?, password=?, email=?, " +
                    "fullname=?, gender=CAST(? AS player_gender), birthday_date=?, registration_time=?, about=?, avatar=? " +
                    "WHERE (players.id=?)")
        ){
            pstmt.setString(1, player.getLogin());
            pstmt.setString(2, player.getPassword());
            pstmt.setString(3, player.getEmail());
            pstmt.setString(4, player.getFullName());
            pstmt.setString(5, player.getGender()!=null ? player.getGender().toString() : null);
            pstmt.setDate(6, player.getBirthdayDate()!=null ?  Date.valueOf(player.getBirthdayDate()) : null);
            pstmt.setTimestamp(7, player.getRegistrationTime()!=null ? Timestamp.valueOf(player.getRegistrationTime()) : null);
            pstmt.setString(8, player.getAbout());
            pstmt.setString(9, player.getAvatar());
            pstmt.setLong(10, player.getId());
            return pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int delete(Player player){
        try(Connection con = getConnection();
            PreparedStatement pstmt = con.prepareStatement("DELETE FROM players WHERE (users.id=?)"
            )
        ){
            pstmt.setLong(1, player.getId());
            return pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int delete(Long id){
        try(Connection con = getConnection();
            PreparedStatement pstmt = con.prepareStatement("DELETE FROM players WHERE (players.id=?)"
            )
        ){
            pstmt.setLong(1, id);
            return pstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Player get(Long playerId){
        Player player = null;
        try(Connection con = getConnection();
            PreparedStatement pstmt = createPreparedStatement(con, playerId);
            ResultSet rs = pstmt.executeQuery()){

            while (rs.next()){
                player = getPlayerFromResultSet(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return player;
    }

    public List<Player> getAll(){
        List<Player> players = new ArrayList<>();
        try(Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM players")) {

            Player player;
            while(rs.next()){
                players.add(getPlayerFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return players;
    }

    public static Connection getConnection() {
        Properties props = new Properties();
        Connection con = null;
        try (FileInputStream fis = new FileInputStream("src/main/resources/db/db.properties")){
            props.load(fis);

            Class.forName(props.getProperty("DB_DRIVER_CLASS"));

            con = DriverManager.getConnection(props.getProperty("DB_URL"),
                    props.getProperty("DB_USERNAME"),
                    props.getProperty("DB_PASSWORD"));
        } catch (IOException | ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return con;
    }

    private Player getPlayerFromResultSet(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        String login = rs.getString("login");
        String email = rs.getString("email");
        String password = rs.getString("password");
        String fullName = rs.getString("fullname");
        Gender gender = rs.getString("gender")!=null ? Gender.valueOf(rs.getString("gender")) : null;
        LocalDate birthdayDate = rs.getDate("birthday_date")!=null ?
                rs.getDate("birthday_date").toLocalDate(): null;
//                LocalDate birthdayDate = rs.getObject(7, LocalDate.class); DOESN'T WORK, ALTHOUGH HAS TO
        LocalDateTime registrationTime = rs.getTimestamp("registration_time")!=null ?
                rs.getTimestamp("registration_time").toLocalDateTime() : null;
        String about = rs.getString("about");
        String avatar = rs.getString("avatar");

        Player player = new Player(id, login, password, email,
                fullName, gender, birthdayDate, registrationTime, about, avatar);
        return player;
    }

    private PreparedStatement createPreparedStatement(Connection con, Long playerId) throws SQLException {
        String sql = "SELECT * FROM players WHERE id=?";
        PreparedStatement pstmt = con.prepareStatement(sql);
        pstmt.setLong(1, playerId);
        return pstmt;
    }
}