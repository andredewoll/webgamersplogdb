package com.softserveinc.ita.multigame.model.engine;


import static com.softserveinc.ita.multigame.model.engine.GameState.*;

public class RockScissorsPaper extends GenericGameEngine<String>{

    /*Additional static variables for using in game logic */
    static final String ROCK = "ROCK";
    static final String SCISSORS = "SCISSORS";
    static final String PAPER = "PAPER";

    static String firstPlayerChoice;
    static String secondPlayerChoice;
    
    public RockScissorsPaper() {
    	super();
    }

    public RockScissorsPaper(Long id) {
        super(id);
    }

    @Override
    public boolean validateTurnSyntax(String turn) {
        return ROCK.equals(turn) || SCISSORS.equals(turn)
                || PAPER.equals(turn);
    }

    @Override
    protected boolean validateTurnLogic(String turn) {
        /*Method is not implemented, because our turn logic is
        * implemented in changeGameState method*/
        return true;
    }

    @Override
    protected boolean validatePlayer(String player) {
        return !(player == null || ((String) player).trim().isEmpty());
    }

    @Override
    protected int changeGameState(String player, String turn) {

        if (gameState == WAIT_FOR_FIRST_PLAYER_TURN) {
            firstPlayerChoice = turn;
            return WAIT_FOR_SECOND_PLAYER_TURN;
        }
        if (gameState == WAIT_FOR_SECOND_PLAYER_TURN) {
            secondPlayerChoice = turn;
        }

        if (firstPlayerWins()) {
            return FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        }

        if (secondPlayerWins()) {
            return FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        }

        if (happensDraw()) {
            return FINISHED_WITH_DRAW;
        }

        else return WAIT_FOR_FIRST_PLAYER_TURN;
    }

    /*Additional static methods for implementing our game logic */
     static boolean firstPlayerWins() {
        return firstPlayerChoice.equals(ROCK) && secondPlayerChoice.equals(SCISSORS)
                || firstPlayerChoice.equals(SCISSORS) && secondPlayerChoice.equals(PAPER)
                || firstPlayerChoice.equals(PAPER) && secondPlayerChoice.equals(ROCK);
    }

     static boolean secondPlayerWins() {
        return firstPlayerChoice.equals(ROCK) && secondPlayerChoice.equals(PAPER)
                || firstPlayerChoice.equals(SCISSORS) && secondPlayerChoice.equals(ROCK)
                || firstPlayerChoice.equals(PAPER) && secondPlayerChoice.equals(SCISSORS);
    }

    static boolean happensDraw() {
        return firstPlayerChoice.equals(ROCK) && secondPlayerChoice.equals(ROCK)
                || firstPlayerChoice.equals(SCISSORS) && secondPlayerChoice.equals(SCISSORS)
                || firstPlayerChoice.equals(PAPER) && secondPlayerChoice.equals(PAPER);
    }

   
}
    




