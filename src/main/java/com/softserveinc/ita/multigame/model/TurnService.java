package com.softserveinc.ita.multigame.model;

public class TurnService {
	
	private GameListManager gameListManager = GameListManager.getInstance();
	
	public boolean makeTurn(Player player, Long id, String turn) {
		return gameListManager.getGameById(id).getGameEngine().makeTurn(player.getLogin(), turn);
	}
	

}
