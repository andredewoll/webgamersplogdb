package com.softserveinc.ita.multigame.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PlayerListManager  {
	private static Map<String, Player> map;
	
	private static PlayerListManager instance = null;
	
	private PlayerListManager() {
		map = new HashMap<String, Player>();
	}
	
	public static PlayerListManager getInstance() {
		if (instance == null) {
			instance = new PlayerListManager();
		} return instance;
	}
	
	public boolean registerNewPlayer(Player player) {
		if (map.containsKey(player.getLogin())) {
		    return false;
		} else {
		    map.put(player.getLogin(), player);
		    return true;
		}
	}

	public boolean contains(Player player) {
		    return map.containsKey(player.getLogin());
	}
	
	public Player getPlayerByName(String name) {
		return (Player) map.get(name);
	}
	
	public List<Player> getAllPlayers() {
		return new LinkedList<Player>(map.values());
	}
	
	public void clear() {
	map.clear();
	}
	
}	
