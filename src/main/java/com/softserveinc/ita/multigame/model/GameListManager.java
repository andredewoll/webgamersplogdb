package com.softserveinc.ita.multigame.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.softserveinc.ita.multigame.model.engine.RockScissorsPaper;
import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;

public class GameListManager  {
	private static Map<Long, Game> map;
	
	private static GameListManager instance = null;
	
	private GameListManager() {
		map = new HashMap<Long, Game>();
	}
	
	public static GameListManager getInstance() {
		if (instance == null) {
			instance = new GameListManager();
		} return instance;
	}
	
	
	
	
	public boolean createGame(Player player) {
		if (player == null) {
			return false;	
		}
		GenericGameEngine<String> rockScissorsPaper = new RockScissorsPaper();
		rockScissorsPaper.setFirstPlayer(player.getLogin());
		Game game = new Game(player, rockScissorsPaper);
		map.put(game.getGameEngine().getId(), game);
		return map.containsKey(game.getGameEngine().getId());
	}
	
	public boolean deleteGame(Long id) {
		map.remove(id);
		 return true;
	}
		
	
	
	public List<Long> getCreatedGamesIds(Player player) {
		List<Long> listCreated = new ArrayList<>();
		for (Map.Entry<Long, Game> entry : map.entrySet()) {
			
			if (entry.getValue().getFirstPlayer() == player
					&& entry.getValue().getSecondPlayer() == null) {
				listCreated.add(entry.getKey());
			} //This means that me (1st) player created game and is waiting for 2nd (somebody else)
		}
		
		return listCreated;
	}
	
	public List<Long> getPlayingGamesIds(Player player) {
		List<Long> listCreated = new ArrayList<>();
		for (Map.Entry<Long, Game> entry : map.entrySet()) {
			
			if (entry.getValue().getFirstPlayer() == player
					&& entry.getValue().getSecondPlayer() != null) {
				listCreated.add(entry.getKey());
			} //This means that 1st & 2nd players are playing, cause secondPlayer != null
		}
		
		return listCreated;
	}
	
	public List<Long> getWaitingGamesIds(Player player) {
		List<Long> listCreated = new ArrayList<>();
		for(Map.Entry<Long, Game> entry : map.entrySet()) {
			if (entry.getValue().getSecondPlayer() == null
					&& entry.getValue().getFirstPlayer() != player) {
				listCreated.add(entry.getKey());
			} //This means that somebody else (1st player) created game and waits for me (2nd) to join
		}
		return listCreated;
	}

	public Game getGameById(Long id) {
		Game game = map.get(id);
		return game;
	}

	public List<Long> getAllGames() {
		return new ArrayList<>(map.keySet());
	}

}
