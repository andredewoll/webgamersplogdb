package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.PlayerListManager;
import com.softserveinc.ita.multigame.model.engine.RockScissorsPaper;

@WebServlet("/Play/*")
public class PlayingGamesController extends HttpServlet {

	private static final long serialVersionUID = -1956285857786588265L;

	PlayerListManager playerManagerService = PlayerListManager.getInstance();
	GameListManager gameManagerService = GameListManager.getInstance();
	RockScissorsPaper rsp = null;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {



		Long id = Long.parseLong(request.getParameter("id"));
		String currentPlayerName = request.getParameter("name");
		rsp = (RockScissorsPaper) gameManagerService.getGameById(id).getGameEngine();
		String enemy ;

		if (gameManagerService.getGameById(id).getFirstPlayer().getLogin().equals(currentPlayerName)) {
			enemy = gameManagerService.getGameById(id).getSecondPlayer().getLogin();
		} else {
			enemy = gameManagerService.getGameById(id).getFirstPlayer().getLogin();
		}

		if (gameManagerService.getGameById(id).getSecondPlayer() == null) {
			gameManagerService.getGameById(id).setSecondPlayer(playerManagerService.
					getPlayerByName(currentPlayerName));
		} else {
			gameManagerService.getGameById(id).
					setSecondPlayer(playerManagerService.getPlayerByName(enemy));
		}

		rsp.setFirstPlayer(currentPlayerName);
		rsp.setSecondPlayer(enemy);
		rsp.isStarted();

		boolean isStarted = gameManagerService.getGameById(id).getGameEngine().isStarted();
		String winner =  gameManagerService.getGameById(id).getGameEngine().getTheWinner();
		int resultCode = gameManagerService.getGameById(id).getGameEngine().getResultCode();
		String gameState = gameManagerService.getGameById(id).getGameEngine().toString();

		request.setAttribute("gameState", gameState);
		request.setAttribute("isStarted", isStarted);
		request.setAttribute("resultCode", resultCode);
		request.setAttribute("winner", winner);
		request.setAttribute("id", id);
		request.setAttribute("currentPlayerName", currentPlayerName);
		request.setAttribute("enemy", enemy);
		ServletContext ctx = getServletContext();
		RequestDispatcher dispatcher = ctx.getRequestDispatcher("/PlayingGames.jsp");
		dispatcher.forward(request, response);
	}
}
