package com.softserveinc.ita.multigame.web.controllers;

import org.apache.log4j.Logger;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Created/*")
public class CreatedGamesController extends HttpServlet {


    private static final long serialVersionUID = 7537651997518425903L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String name = request.getParameter("name");

        request.setAttribute("id", id);
        request.setAttribute("name", name );

        // ServletContext ctx = getServletContext();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/CreatedGames.jsp");
        dispatcher.forward(request, response);
    }
}
