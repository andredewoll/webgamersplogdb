package com.softserveinc.ita.multigame.web.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayerListManager;
import org.apache.log4j.Logger;

import java.io.IOException;

@WebServlet("/GameCreation")
public class GameCreationController extends HttpServlet {

	private static final long serialVersionUID = -6480559953705550114L;

	PlayerListManager playerManagerService = PlayerListManager.getInstance();
	GameListManager gameListManagerService = GameListManager.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String currentPlayerName = request.getParameter("name");
		Player currentPlayer = playerManagerService.getPlayerByName(currentPlayerName);
		gameListManagerService.createGame(currentPlayer);

		request.setAttribute("currentPlayer", currentPlayerName);

		ServletContext ctx = getServletContext();
		RequestDispatcher dispatcher = ctx.getRequestDispatcher("/List");
		dispatcher.forward(request, response);



	}



}
