package com.softserveinc.ita.multigame.model.engine;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestValidationPlayerName {

    @Test
    public void testValidationWhenPlayerNameIsNull() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String playerName = null;

        boolean actual = rockScissorsPaper.validatePlayer(playerName);

        assertFalse(actual);
    }

    @Test
    public void testValidationWhenPlayerNameIsNotNull() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String playerName = "I am not NULL!!!";

        boolean actual = rockScissorsPaper.validatePlayer(playerName);

        assertTrue(actual);

    }

    @Test
    public void testValidationWhenPlayerNameIsEmptyString() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String playerName = "";

        boolean actual =  rockScissorsPaper.validatePlayer(playerName);

        assertFalse(actual);
    }

    @Test
    public void testValidationWhenPlayerNameIsNotEmptyString() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String playerName = "I am not Empty   ";

        boolean actual =  rockScissorsPaper.validatePlayer(playerName);

        assertTrue(actual);
    }



}
