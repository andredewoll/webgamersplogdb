package com.softserveinc.ita.multigame.model;


import java.lang.reflect.Field;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GameListManagerTest {
	
	private GameListManager manager;
	private Player player1;
	private Player player2;
	
	private static Game firstGame;
	private static Game secondGame;
       
    @Before
    public void setUp() {
	manager = GameListManager.getInstance();
	player1 = mock(Player.class, Mockito.CALLS_REAL_METHODS);
	player2 = mock(Player.class, Mockito.CALLS_REAL_METHODS);
	player1.setLogin("player1");
	player2.setLogin("player2");
	when(player1.getLogin()).thenReturn("player1");
	when(player2.getLogin()).thenReturn("player2");
    }
    
    @After
    public void tireDown() {
    	try {
    	    Field field = GameListManager.class.getDeclaredField("instance");
    	    field.setAccessible(true);
    	    field.set(null, null);
    	} catch (Exception e) {
    	    fail("Field \"manager\" is not accessible");
    	}
    	try {
    	    Field field = GameListManager.class.getDeclaredField("map");
    	    field.setAccessible(true);
    	    field.set(null, null);
    	} catch (Exception e) {
    	    fail("Field \"map\" is not accessible");
    	}
    	
        }
    
    @Test
    public void managerIsSingleton() {
	GameListManager newManager = GameListManager.getInstance();
	assertSame(newManager, manager);
    }
    
    @Test
    public void gameIsCreatedWithPlayerNotNull() {
    	assertTrue(manager.createGame(player1));
    }
    
    @Test
    public void gameIsNotCreatedWithPlayerIsNull() {
    	assertFalse(manager.createGame(null));
    }
    
    @Test()
    public void gameCanBeDeletedIfPlayersWereAdded() {
    	manager.createGame(player2);
    	assertTrue(manager.deleteGame(1L));
    }
    
    @Test
    public void newlyCreatedManagerShouldBeEmpty() {
        assertEquals(0, manager.getAllGames().size());
    }
    
    @Test
    public void managerContainsAsManyGamesAsHaveBeenAdded() {
    	fillManagerWithGames();
    	assertEquals(2, manager.getAllGames().size());
    }  
        
    private void fillManagerWithGames() {
        manager.createGame(player1);
    	manager.createGame(player2);
    }
   
    

}
