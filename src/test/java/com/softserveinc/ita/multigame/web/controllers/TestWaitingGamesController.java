package com.softserveinc.ita.multigame.web.controllers;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.model.GameListManager;

public class TestWaitingGamesController extends Mockito {
	 @Mock
	 private HttpServletRequest request; 
	 @Mock
	 private HttpServletResponse response; 
	 @Mock
	 private RequestDispatcher dispatcher; 
	 @Mock
	 private GameListManager gameListManagerService = GameListManager.getInstance()  ;
	

	 private WaitingGamesController controller;
	 
	 @Before
	 public void setUp() throws Exception {
	  MockitoAnnotations.initMocks(this);
	 controller = new WaitingGamesController();
	 }
	 
	 @Test(expected = NullPointerException.class)
	 public void test() throws ServletException, IOException {
		 when(request.getParameter("name")).thenReturn("name");
		 when(request.getParameter("id")).thenReturn("id");
		 when(request.getRequestDispatcher("/WaitingGames.jsp")).thenReturn(dispatcher);
		 when(gameListManagerService.getGameById(1L)).thenReturn(anyObject());
		 when(gameListManagerService.getGameById(1L).getFirstPlayer()).thenReturn(anyObject());
		 when(gameListManagerService.getGameById(1L).getFirstPlayer().getLogin()).thenReturn(anyString());
		 
		 controller.doGet(request, response);
		 verify(request.getParameter("name"));
		 verify(request.getParameter("id"));
		 verify((gameListManagerService.getGameById(1L).getFirstPlayer().getLogin()));
	 }
	 
	 

}
