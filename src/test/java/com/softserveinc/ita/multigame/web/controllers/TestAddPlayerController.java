package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.lang.reflect.Field;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayerListManager;


public class TestAddPlayerController extends Mockito {
	
	 @Mock
	 private HttpServletRequest request; 
	 @Mock
	 private HttpServletResponse response; 
	 @Mock
	 private RequestDispatcher dispatcher; 
	 @Mock
	 private PlayerListManager playerManagerService; 
	 @Mock
	 private Player player; 
	 
	 private AddPlayerController controller;
	 
	 @Before
	 public void setUp() throws Exception {
	  MockitoAnnotations.initMocks(this);
	  controller = new AddPlayerController();
	 }  
	
	 
	 @Test(expected = IllegalStateException.class)
	 public void test() throws ServletException, IOException {
		 when(request.getParameter("name")).thenReturn("name");
		 when(request.getParameter("playerFullName")).thenReturn("playerFullName");
		 when(request.getParameter("save")).thenReturn("save");
		 when(request.getParameter("cancel")).thenReturn("cancel");
		 when(request.getRequestDispatcher("/login.html")).thenReturn(dispatcher);
		 when(request.getRequestDispatcher("/ListOfPlayers.jsp")).thenReturn(dispatcher);
		 when(request.getRequestDispatcher("/errorRegistration.html")).thenReturn(dispatcher);
		 when(playerManagerService.registerNewPlayer(player)).thenReturn(true);
		 
		 controller.doPost(request, response);
		 verify(playerManagerService.registerNewPlayer(anyObject()));
		 verify(request.getParameter("name"));
		 verify(request.getParameter("playerFullName"));
		 verify(request.getParameter("save"));
		 verify(request.getParameter("cancel"));
		 
		 
		 
	 }

}
